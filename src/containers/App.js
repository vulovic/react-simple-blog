import React from "react";
import {Route, Switch, BrowserRouter} from "react-router-dom";
import { browserHistory } from 'react-router';

import Home from "../components/Home";
import Other from "../components/Other";
import NotFound from "../components/NotFound";
import Menu from "../components/Menu";
import Posts from "../components/Posts";
import Post from "../components/Post";

export default class App extends React.Component{
    render(){
        return(
            <BrowserRouter history={browserHistory}>
            <div>
                <Menu />
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/other" component={Other}/>
                    <Route exact path="/posts" component={Posts}/>
                    <Route path="/posts/:id" component={Post} {...this.props.match}/>
                    <Route component={NotFound}/>
                </Switch>
            </div>
            </BrowserRouter>
        );
    }
}