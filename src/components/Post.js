import React from "react";
import {getPost} from "../actions/postActions";
import {connect} from "react-redux";

class Post extends React.Component{

    componentDidMount(){
        this.props.getPost(this.props.match.params.id);
    }

    render(){
        return (
            <div>
                <h1>{this.props.post.post.title}</h1>
                <p>{this.props.post.post.body}</p>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        post: state.postReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getPost:(id)=>{
            dispatch(getPost(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post);