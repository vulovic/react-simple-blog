import React from "react";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {getAllPosts} from "../actions/postActions";

class Posts extends React.Component{

    componentDidMount(){
        this.props.getAllPosts();
    }

    render(){
        var postLinks = this.props.post.posts.map((post, index) => (
            <div className={"link"} key={index}>
                <NavLink to={`/posts/${post.id}`}>{post.title}</NavLink>
            </div>
        ))
        return (
            <div>
                <h3>Your Posts:</h3>
                {postLinks}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        post: state.postReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAllPosts:()=>{
            dispatch(getAllPosts());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Posts);