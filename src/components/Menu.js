import React from "react";
import {NavLink} from "react-router-dom";

export default class Menu extends React.Component{
    render(){
        return(
            <div>
                <ul className={"menu"}>
                    <li className={"link"}><NavLink exact activeClassName="activeLink" to="/">Home</NavLink></li>
                    <li className={"link"}><NavLink activeClassName="activeLink" to="/other">Other</NavLink></li>
                    <li className={"link"}><NavLink activeClassName="activeLink" to="/posts">Posts</NavLink></li>
                </ul>
            </div>
        );
    }
}