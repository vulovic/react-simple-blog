import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import postReducer from "./reducers/postReducer";
import {getAllPosts} from "./actions/postActions";


const error = (store) => (next) => (action) =>{
    try{ next(action)	}
    catch(err){	console.log("Caught an exception: ",err)	}
}

const store = createStore(combineReducers({postReducer}), {}, applyMiddleware(promise(), thunk, logger, error));

store.dispatch(getAllPosts());

export default store;