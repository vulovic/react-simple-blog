const postReducer = (state = {
    posts:[],
    post:{},
    error: null
}, action)=>{
    switch(action.type){
        case "GET_ALL_POSTS":
            state = {	...state }
            break;
        case "GET_ALL_POSTS_FULFILLED":
            state = { ...state, posts:action.payload.data}
            break;
        case "GET_ALL_POSTS_REJECTED":
            state = { ...state, error:action.payload}
            break;

        case "GET_POST":
            state = {	...state }
            break;
        case "GET_POST_FULFILLED":
            state = { ...state, post:action.payload.data}
            break;
        case "GET_POST_REJECTED":
            state = { ...state, error:action.payload}
            break;

        default: break;
    }
    return state;
}
export default postReducer;