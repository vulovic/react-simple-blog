import axios from "axios";

export function getAllPosts() {
    const request = axios.get(`http://localhost:3000/posts`);
    return {
        type: "GET_ALL_POSTS",
        payload: request
    }
}

export function getPost(id) {
    const request = axios.get(`http://localhost:3000/posts/${id}`);
    return {
        type: "GET_POST",
        payload: request
    }
}