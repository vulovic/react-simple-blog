/**
 * File for importing other js, css, scss files (without underscore prefix). These files will be bundled at the end
 * Files with underscore prefix should be imported in scss file without leading underscore
 */
import "../css/style.scss";
import "../app/index.js";